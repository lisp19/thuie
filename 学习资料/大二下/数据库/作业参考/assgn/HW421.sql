DROP table IF EXISTS person;
create table person (
	ID CHAR(10),
	name CHAR(40),
	mother CHAR(10),
	father CHAR(10),
	PRIMARY KEY (ID),
	FOREIGN KEY (father) REFERENCES person ON UPDATE CASCADE,
	FOREIGN KEY (mother) REFERENCES person ON UPDATE CASCADE
);
DROP FUNCTION IF EXISTS insertfamily(character,character,character,character,character,character,character,character);
create or replace function insertfamily(ID1 char(10),Child1 char(10), ID2 char(10), Child2 char(10), ID3 char(10), Mother char(10), ID4 char(10),Father char(10)) returns int as $$
BEGIN
INSERT INTO person(ID, name, mother, father) VALUES 
	(ID3, Mother, null, null),
	(ID4, Father, null, null);
INSERT INTO person(ID, name, mother, father) VALUES 
	(ID1, Child1, ID3, ID4 ),
	(ID2, Child2, ID3, ID4 );
return(SELECT count(*) FROM person);
END
$$ 
language plpgsql;
select insertfamily('231','AMao','341','AGo','33','Lao','43','Te');

create or replace function del_parents() 
returns trigger as 
$$
declare n int;
begin
select count(*) into n from person where person.mother=OLD.mother;
	if n=0
	then
		delete from person where person.ID=OLD.mother;
		delete from person where person.ID=OLD.father;
	end if;
return null;
end
$$
language plpgsql;

create trigger del_parents 
after delete on person 
for each row 
execute procedure del_parents();

DELETE FROM person WHERE ID='231';
DELETE FROM person WHERE ID='341';
SELECT * FROM person; 
