--drop tables
DROP TABLE IF EXISTS forum_user CASCADE;
DROP TABLE IF EXISTS article CASCADE;
DROP TABLE IF EXISTS comment CASCADE;
--create tables
CREATE TABLE forum_user(
	username VARCHAR(50),
	password VARCHAR(50),
	age INTEGER,
	location VARCHAR(50),
	PRIMARY KEY (username)
);

CREATE TABLE article(
	article_id INTEGER,
	title VARCHAR(50),
	content VARCHAR(50),
	create_by VARCHAR(50),
	create_at TIMESTAMP,
	PRIMARY KEY (article_id),
	FOREIGN KEY (create_by) REFERENCES forum_user(username)
);

CREATE TABLE comment(
	comment_id INTEGER,
	article_id INTEGER,
	content VARCHAR(50),
	create_by VARCHAR(50),
	create_at TIMESTAMP,
	PRIMARY KEY (comment_id),
	FOREIGN KEY (create_by) REFERENCES forum_user(username)
);

--insert data
INSERT INTO forum_user(username,password,age, location) VALUES
	('Alice','md5{202cb962ac59075b964b07152d234b70}',20,'USA'),
	('Bob','md5{250cf8b51c773f3f8dc8b4be867a9a02}',23,'UK'),
	('David','md5{68053af2923e00204c3ca7c6a3150cf7}',45,'USA'),
	('John','md5{d2490f048dc3b77a457e3e450ab4eb38}',32,'Canada');
	
INSERT INTO article(article_id,title,content,create_by,create_at) VALUES
	(1,'Homework','It is easy!','David','2020-03-16 08:58:00'),
	(2,'Busy','I want more time.','Bob','2020-03-14 12:32:00'),
	(3,'Hello','This is my first article.','John','2020-03-10 09:13:00'),
	(4,'No Title','No Content.','John','2020-03-15 10:34:00');
	
INSERT INTO comment(comment_id,article_id,content,create_by,create_at) VALUES
	(1,1,'Are you kidding?','Alice','2020-03-16 08:59:00'),
	(2,2,'Me too.','Alice','2020-03-14 12:40:00'),
	(3,3,'Welcome!','Alice','2020-03-11 01:03:00'),
	(4,4,'What?','Alice','2020-03-15 10:38:00'),
	(5,1,'I don''t think so!','John','2020-03-16 09:05:00'),
	(6,2,'I have time~','David','2020-03-14 13:12:00');
	
--select
DELETE FROM article 
WHERE create_at=(SELECT MIN(create_at) FROM article WHERE create_by='John');
SELECT * FROM article