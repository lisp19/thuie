%点云数据预处理（坐标旋转）
A = cursor_info(4).Position; 
B = cursor_info(3).Position;
C = cursor_info(2).Position;
D = cursor_info(1).Position;
AB = B-A;
AC = C-A;
BC = C-B;
v3 = cross(AB, AC);
v3 = v3/norm(v3);
v1 = BC/norm(BC);
v2 = cross(v1,v3);
u1 = [1 0 0];
u2 = [0 1 0];
u3 = [0 0 1];
T = inv([v1;v2;v3])*[u1;u2;u3];
vf = v * T;
x = vf(:,1);
y = vf(:,2);
z = vf(:,3);
A1 = A * T;
B1 = B * T;
C1 = C * T;
D1 = D * T;
plot3(x,y,z,'b.'); %通过散点图，判断手臂是否基本竖直（即与z轴平行），如有偏差，返回Sign脚本，重新标记关键点；
save([name,'_R.mat'],'vf'); %存储旋转坐标后的点云数据

%截取5mm内的数据点作为分析单元
upvalue01 = A1(3)+2.5;
downvalue01 = A1(3)-2.5;
temp01 = find(z>=downvalue01 & z<= upvalue01);
zcross = z(temp01);
crosssection01 = vf(ismember(vf(:,3),zcross),:);
x1 = crosssection01(:,1);
y1 = crosssection01(:,2);
z1 = crosssection01(:,3);
hold on;
plot3(x1,y1,z1,'g.'); %绘制三维点云图
crosssection01(:,3) = []; %投影到x-y平面
A2 = A1(1:2);
B2 = B1(1:2);
C2 = C1(1:2);
D2 = D1(1:2);
crosssection01(:,2) = crosssection01(:,2) - B2(2);
D2(:,2) = D2(:,2)-C2(2);
B2(:,2) = B2(:,2)-B2(2);
C2(:,2) = C2(:,2)-C2(2);

%将点划分为两组，获得yplus和yminus
temp03 = find(crosssection01(:,2) >= 0);
temp04 = find(crosssection01(:,2) < 0);
yplus = crosssection01(temp03,:);
yminus = crosssection01(temp04,:);

%分别对两组数据进行拟合，获得fx和gx
x4 = yplus(:,1);
y4 = yplus(:,2);
x5 = yminus(:,1);
y5 = yminus(:,2);
fx = polyfit(x4,y4,6);
gx = polyfit(x5,y5,6);

%计算截面A测点的坐标
temp05 = roots(fx);
B3 = [temp05(6),0];%拟合曲线与x轴交点，靠近大拇指；
C3 = [temp05(1),0];%拟合曲线与x轴交点，靠近小指；
halfaxis = (B3(1)+C3(1))/2;%二分之一横坐标
quarter = (B3(1)+halfaxis)/2;%四分之一横坐标
threequarter = (halfaxis+C3(1))/2;%四分之三横坐标
eighth = (B3(1)+quarter)/2;%八分之一横坐标
seveneighth = (threequarter+C3(1))/2;%八分之七横坐标
A3 = [halfaxis,polyval(gx,halfaxis)];
point01 = [eighth, polyval(gx,eighth)];
point02 = [quarter, polyval(gx,quarter)];
point03 = [threequarter, polyval(gx,threequarter)];
point04 = [seveneighth, polyval(gx,seveneighth)];
point05 = [eighth, polyval(fx,eighth)];
point06 = [quarter, polyval(fx,quarter)];
point07 = [threequarter, polyval(fx,threequarter)];
point08 = [seveneighth, polyval(fx,seveneighth)];
point09 = [halfaxis,polyval(fx,halfaxis)];
keypoint_A = [A1 B1 C1 D1 A2 B2 C2 A3 B3 C3 D2 point01 point02 point03 point04 point05 point06 point07 point08 point09];

%存储截面A测点的坐标
save([name,'_kp_A.mat'],'keypoint_A');

%计算截面A相关测量项目（即尺寸）
longaxis_A = norm(B3-C3); %长轴长度
shortaxis_A = norm(A3-point09); %短轴长度
R1 = ThreePoint2Circle(B3,point01,point02); %左：桡骨侧背面弧度；右：尺骨侧背面弧度
R2 = ThreePoint2Circle(A3,point02,point03); %左右：手腕背面弧度；
R3 = ThreePoint2Circle(C3,point03,point04); %左：尺骨侧背面弧度；右：桡骨侧背面弧度
R4 = ThreePoint2Circle(B3,point05,point06); %左：桡骨侧腹面弧度；右：尺骨侧腹面弧度
R5 = ThreePoint2Circle(point06,point07,point08); %左右：手腕腹面弧度
R6 = ThreePoint2Circle(C3,point08,point09); %左：尺骨侧腹面弧度；右：桡骨侧腹面弧度
height = 100; %计算尺骨茎突点高
for i=1:size(crosssection01,1)
    temp06 = crosssection01(i,:);
    temp07 = norm(D2-temp06);
    if temp07 < height
        height = temp07;
    end
end
relativeposition = D2-A3;
xvalue = B3(1):0.1:C3(1);
yvalue1 = polyval(fx,xvalue);
yvalue2 = polyval(gx,xvalue);
d1 = sqrt(diff(xvalue).^2+diff(yvalue1).^2);
d2 = sqrt(diff(xvalue).^2+diff(yvalue2).^2);
L_A = sum(d1)+sum(d2); %腕围

%存储截面A相关测量项目（即尺寸）
keydimension_A = [longaxis_A shortaxis_A R1 R2 R3 R4 R5 R6 height L_A];
save([name,'_kd_A.mat'],'keydimension_A');

%截取截面A向下平移4cm后，5mm内的数据点
upvalue02 = A1(3)-40+2.5;
downvalue02 = A1(3)-40-2.5;
temp08 = find(z>=downvalue02 & z<= upvalue02);
zcross = z(temp08);
crosssection02 = vf(ismember(vf(:,3),zcross),:);
x6 = crosssection02(:,1);
y6 = crosssection02(:,2);
z2 = crosssection02(:,3);
hold on;
plot3(x6,y6,z2,'k.'); %绘制三维点云图
crosssection02(:,3) = []; %投影到x-y平面
A4 = A1(1:2);
B4 = B1(1:2);
C4 = C1(1:2);
D4 = D1(1:2);
crosssection02(:,2) = crosssection02(:,2) - B4(2);
D4(:,2) = D4(:,2)-C4(2);
B4(:,2) = B4(:,2)-B4(2);
C4(:,2) = C4(:,2)-C4(2);

%将点划分为两组，获得yplus和yminus
temp09 = find(crosssection02(:,2) >= 0);
temp10 = find(crosssection02(:,2) < 0);
yplus01 = crosssection02(temp09,:);
yminus01 = crosssection02(temp10,:);

%分别对两组数据进行拟合，获得fx和gx
x9 = yplus01(:,1);
y9 = yplus01(:,2);
x10 = yminus01(:,1);
y10 = yminus01(:,2);
fx01 = polyfit(x9,y9,6);
gx01 = polyfit(x10,y10,6);

%计算截面B测点的坐标
temp11 = roots(fx01);
for i=1:3
    if imag(temp11(i)) == 0 && real(temp11(i))>0
        B5temp = [temp11(i),0];
    end
end
for i=4:6
    if imag(temp11(i)) == 0 && real(temp11(i))>0
        C5temp = [temp11(i),0];
    end
end
if B5temp(1)>C5temp(1)
    B5 = C5temp;
    C5 = B5temp;
else
    B5 = B5temp;
    C5 = C5temp;
end
halfaxis01 = (B5(1)+C5(1))/2;%二分之一横坐标
quarter01 = (B5(1)+halfaxis01)/2;%四分之一横坐标
threequarter01 = (halfaxis01+C5(1))/2;%四分之三横坐标
eighth01 = (B5(1)+quarter01)/2;%八分之一横坐标
seveneighth01 = (threequarter01+C5(1))/2;%八分之七横坐标
A5 = [halfaxis01,polyval(gx01,halfaxis01)];
point10 = [eighth01, polyval(gx01,eighth01)];
point11 = [quarter01, polyval(gx01,quarter01)];
point12 = [threequarter01, polyval(gx01,threequarter01)];
point13 = [seveneighth01, polyval(gx01,seveneighth01)];
point14 = [eighth01, polyval(fx01,eighth01)];
point15 = [quarter01, polyval(fx01,quarter01)];
point16 = [threequarter01, polyval(fx01,threequarter01)];
point17 = [seveneighth01, polyval(fx01,seveneighth01)];
point18 = [halfaxis01,polyval(fx01,halfaxis01)];
keypoint_B = [A1 B1 C1 D1 A2 B2 C2 A5 B5 C5 D2 point10 point11 point12 point13 point14 point15 point16 point17 point18];

%存储截面B测点的坐标
save([name,'_kp_B.mat'],'keypoint_B');

%计算截面B相关测量项目（即尺寸）
longaxis_B = norm(B5-C5); %长轴长度
shortaxis_B = norm(A5-point18); %短轴长度
R7 = ThreePoint2Circle(B5,point10,point11); %左：桡骨侧背面弧度；右：尺骨侧背面弧度
R8 = ThreePoint2Circle(A5,point11,point12); %左右：手腕背面弧度；
R9 = ThreePoint2Circle(C5,point12,point13); %左：尺骨侧背面弧度；右：桡骨侧背面弧度
R10 = ThreePoint2Circle(B5,point14,point15); %左：桡骨侧腹面弧度；右：尺骨侧腹面弧度
R11 = ThreePoint2Circle(point15,point16,point17); %左右：手腕腹面弧度
R12 = ThreePoint2Circle(C5,point17,point18); %左：尺骨侧腹面弧度；右：桡骨侧腹面弧度
xvalue01 = B5(1):0.1:C5(1);
yvalue3 = polyval(fx01,xvalue01);
yvalue4 = polyval(gx01,xvalue01);
d3 = sqrt(diff(xvalue01).^2+diff(yvalue3).^2);
d4 = sqrt(diff(xvalue01).^2+diff(yvalue4).^2);
L_B = sum(d3)+sum(d4); %腕围

%存储截面B相关测量项目（即尺寸）
keydimension_B = [longaxis_B shortaxis_B R7 R8 R9 R10 R11 R12 L_B];
save([name,'_kd_B.mat'],'keydimension_B');