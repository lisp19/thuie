%点云数据转动
A = cursor_info(4).Position; 
B = cursor_info(3).Position;
C = cursor_info(2).Position;
D = cursor_info(1).Position;
AB = B-A;
AC = C-A;
BC = C-B;
v3 = cross(AB, AC);
v3 = v3/norm(v3);
v1 = BC/norm(BC);
v2 = cross(v1,v3);
u1 = [1 0 0];
u2 = [0 1 0];
u3 = [0 0 1];
T = inv([v1;v2;v3])*[u1;u2;u3];
vf = v * T;
x = vf(:,1);
y = vf(:,2);
z = vf(:,3);
A1 = A * T;
B1 = B * T;
C1 = C * T;
D1 = D * T;
plot3(x,y,z,'b.');%通过散点图，判断手臂是否基本竖直（即与z轴平行），如有偏差，返回Sign01，重新标记关键点；
%save D:\科研\01-华为手腕\01-手腕数据\关键点标记操作\Data\F010N_L.mat vf;
save([name,'_1.mat'],'vf')

%截取5mm内的数据点
upvalue01 = A1(3)+2.5;
downvalue01 = A1(3)-2.5;
temp01 = find(z>=downvalue01 & z<= upvalue01);
zcross = z(temp01);
crosssection01 = vf(ismember(vf(:,3),zcross),:);
x1 = crosssection01(:,1);
y1 = crosssection01(:,2);
z1 = crosssection01(:,3);
hold on;
plot3(x1,y1,z1,'g.');%绘制三维投影图
crosssection01(:,3) = [];%投影到x，y
% x2 = crosssection01(:,1);
% y2 = crosssection01(:,2);
% plot(x2,y2,'g.');%绘制二维投影图
A2 = A1(1:2);
B2 = B1(1:2);
C2 = C1(1:2);
D2 = D1(1:2);
crosssection01(:,2) = crosssection01(:,2) - B2(2);
D2(:,2) = D2(:,2)-C2(2);
B2(:,2) = B2(:,2)-B2(2);
C2(:,2) = C2(:,2)-C2(2);
% x3 = crosssection01(:,1);
% y3 = crosssection01(:,2);
% plot(x3,y3,'g.');%绘制二维投影图

%将点划分为两组，获得yplus和yminus
temp03 = find(crosssection01(:,2) >= 0);
temp04 = find(crosssection01(:,2) < 0);
yplus = crosssection01(temp03,:);
yminus = crosssection01(temp04,:);

%分别对两组数据进行拟合，获得fx和gx
x4 = yplus(:,1);
y4 = yplus(:,2);
x5 = yminus(:,1);
y5 = yminus(:,2);
%plot(x4,y4,'g.');
%plot(x5,y5,'g.');
fx = polyfit(x4,y4,6);
gx = polyfit(x5,y5,6);

%求解所有的关键点
temp05 = roots(fx);
B3 = [temp05(6),0];%拟合曲线与x轴交点，靠近大拇指；
C3 = [temp05(1),0];%拟合曲线与x轴交点，靠近小指；
halfaxis = (B3(1)+C3(1))/2;%二分之一横坐标
quarter = (B3(1)+halfaxis)/2;%四分之一横坐标
threequarter = (halfaxis+C3(1))/2;%四分之三横坐标
eighth = (B3(1)+quarter)/2;%八分之一横坐标
seveneighth = (threequarter+C3(1))/2;%八分之七横坐标
A3 = [halfaxis,polyval(gx,halfaxis)];%手背端二分之一点
point01 = [eighth, polyval(gx,eighth)];%手背端四分之一点，靠近大拇指
point02 = [quarter, polyval(gx,quarter)];%手背端四分之一点，靠近大拇指
point03 = [threequarter, polyval(gx,threequarter)];%手背端四分之三点，靠近小指
point04 = [seveneighth, polyval(gx,seveneighth)];%手背端八分之七点，靠近小指
point05 = [eighth, polyval(fx,eighth)];%手心端四分之一点，靠近大拇指
point06 = [quarter, polyval(fx,quarter)];%手心端四分之一点，靠近大拇指
point07 = [threequarter, polyval(fx,threequarter)];%手心端四分之三点，靠近小指
point08 = [seveneighth, polyval(fx,seveneighth)];%手心端八分之七点，靠近小指
point09 = [halfaxis,polyval(fx,halfaxis)];%手心端二分之一点
keypoint = [A1 B1 C1 D1 A2 B2 C2 A3 B3 C3 D2 point01 point02 point03 point04 point05 point06 point07 point08 point09];

%存储关键点
%save('D:\科研\01-华为手腕\01-手腕数据\关键点标记操作\Data\CP01_F010N_L.mat','A3','B3','C3','D2','point01','point02','point03','point04','point05','point06','point07','point08','point09');
save([name,'_2.mat'],'keypoint');

%关键尺寸提取
longaxis = norm(B3-C3);
shortaxis = norm(A3-point09);
R1 = ThreePoint2Circle(B3,point01,point02);%
R2 = ThreePoint2Circle(A3,point02,point03);
R3 = ThreePoint2Circle(C3,point03,point04);
R4 = ThreePoint2Circle(B3,point05,point06);
R5 = ThreePoint2Circle(point06,point07,point08);
R6 = ThreePoint2Circle(C3,point08,point09);
height = 100;%尺骨茎突方法
for i=1:size(crosssection01,1)
    temp06 = crosssection01(i,:);
    temp07 = norm(D2-temp06);
    if temp07 < height
        height = temp07;
    end
end
relativeposition = D2-A3;
xvalue = B3(1):0.1:C3(1);
yvalue1 = polyval(fx,xvalue);
yvalue2 = polyval(gx,xvalue);
d1 = sqrt(diff(xvalue).^2+diff(yvalue1).^2);
d2 = sqrt(diff(xvalue).^2+diff(yvalue2).^2);
L = sum(d1)+sum(d2);

%存储关键尺寸
%save ('D:\科研\01-华为手腕\01-手腕数据\关键点标记操作\Data\CD01_F010N_L.mat','longaxis','shortaxis','R1','R2','R3','R4','R5','R6','height','relativeposition');
keydimension = [longaxis shortaxis R1 R2 R3 R4 R5 R6 height relativeposition L];
save([name,'_3.mat'],'keydimension');

%截取40mm往下的5mm内的数据点
upvalue02 = A1(3)-40+2.5;
downvalue02 = A1(3)-40-2.5;
temp08 = find(z>=downvalue02 & z<= upvalue02);
zcross = z(temp08);
crosssection02 = vf(ismember(vf(:,3),zcross),:);
x6 = crosssection02(:,1);
y6 = crosssection02(:,2);
z2 = crosssection02(:,3);
hold on;
plot3(x6,y6,z2,'k.');%绘制三维投影图
crosssection02(:,3) = [];%投影到x，y
% x7 = crosssection02(:,1);
% y7 = crosssection02(:,2);
% plot(x7,y7,'g.');%绘制二维投影图
A4 = A1(1:2);
B4 = B1(1:2);
C4 = C1(1:2);
D4 = D1(1:2);
crosssection02(:,2) = crosssection02(:,2) - B4(2);
D4(:,2) = D4(:,2)-C4(2);
B4(:,2) = B4(:,2)-B4(2);
C4(:,2) = C4(:,2)-C4(2);
% x8 = crosssection02(:,1);
% y8 = crosssection02(:,2);
% plot(x8,y8,'g.');%绘制二维投影图

%将点划分为两组，获得yplus和yminus
temp09 = find(crosssection02(:,2) >= 0);
temp10 = find(crosssection02(:,2) < 0);
yplus01 = crosssection02(temp09,:);
yminus01 = crosssection02(temp10,:);

%分别对两组数据进行拟合，获得fx和gx
x9 = yplus01(:,1);
y9 = yplus01(:,2);
x10 = yminus01(:,1);
y10 = yminus01(:,2);
% plot(x9,y9,'g.');
% hold on;
% plot(x10,y10,'g.');
fx01 = polyfit(x9,y9,6);
gx01 = polyfit(x10,y10,6);

%求解所有的关键点
temp11 = roots(fx01);
for i=1:3
    if imag(temp11(i)) == 0 && real(temp11(i))>0
        B5temp = [temp11(i),0];
    end
end
for i=4:6
    if imag(temp11(i)) == 0 && real(temp11(i))>0
        C5temp = [temp11(i),0];
    end
end
if B5temp(1)>C5temp(1)
    B5 = C5temp;
    C5 = B5temp;
else
    B5 = B5temp;
    C5 = C5temp;
end
% B5 = [temp11(6),0];%拟合曲线与x轴交点，靠近大拇指；
% C5 = [temp11(1),0];%拟合曲线与x轴交点，靠近小指；
halfaxis01 = (B5(1)+C5(1))/2;%二分之一横坐标
quarter01 = (B5(1)+halfaxis01)/2;%四分之一横坐标
threequarter01 = (halfaxis01+C5(1))/2;%四分之三横坐标
eighth01 = (B5(1)+quarter01)/2;%八分之一横坐标
seveneighth01 = (threequarter01+C5(1))/2;%八分之七横坐标
A5 = [halfaxis01,polyval(gx01,halfaxis01)];%手背端二分之一点
point10 = [eighth01, polyval(gx01,eighth01)];%手背端四分之一点，靠近大拇指
point11 = [quarter01, polyval(gx01,quarter01)];%手背端四分之一点，靠近大拇指
point12 = [threequarter01, polyval(gx01,threequarter01)];%手背端四分之三点，靠近小指
point13 = [seveneighth01, polyval(gx01,seveneighth01)];%手背端八分之七点，靠近小指
point14 = [eighth01, polyval(fx01,eighth01)];%手心端四分之一点，靠近大拇指
point15 = [quarter01, polyval(fx01,quarter01)];%手心端四分之一点，靠近大拇指
point16 = [threequarter01, polyval(fx01,threequarter01)];%手心端四分之三点，靠近小指
point17 = [seveneighth01, polyval(fx01,seveneighth01)];%手心端八分之七点，靠近小指
point18 = [halfaxis01,polyval(fx01,halfaxis01)];%手心端二分之一点
keypoint01 = [A1 B1 C1 D1 A2 B2 C2 A5 B5 C5 D2 point10 point11 point12 point13 point14 point15 point16 point17 point18];

%存储关键点
%save('D:\科研\01-华为手腕\01-手腕数据\关键点标记操作\Data\CP01_F010N_L.mat','A3','B3','C3','D2','point01','point02','point03','point04','point05','point06','point07','point08','point09');
save([name,'_4.mat'],'keypoint01');

%关键尺寸提取
longaxis01 = norm(B5-C5);
shortaxis01 = norm(A5-point18);
R7 = ThreePoint2Circle(B5,point10,point11);
R8 = ThreePoint2Circle(A5,point11,point12);
R9 = ThreePoint2Circle(C5,point12,point13);
R10 = ThreePoint2Circle(B5,point14,point15);
R11 = ThreePoint2Circle(point15,point16,point17);
R12 = ThreePoint2Circle(C5,point17,point18);
% height01 = 100;%尺骨茎突方法
% for i=1:size(crosssection02,1)
%     temp12 = crosssection02(i,:);
%     temp13 = norm(D2-temp12);
%     if temp13 < height01
%         height01 = temp13;
%     end
% end
%relativeposition = D2-A5;
xvalue01 = B5(1):0.1:C5(1);
yvalue3 = polyval(fx01,xvalue01);
yvalue4 = polyval(gx01,xvalue01);
d3 = sqrt(diff(xvalue01).^2+diff(yvalue3).^2);
d4 = sqrt(diff(xvalue01).^2+diff(yvalue4).^2);
L01 = sum(d3)+sum(d4);

%存储关键尺寸
%save ('D:\科研\01-华为手腕\01-手腕数据\关键点标记操作\Data\CD01_F010N_L.mat','longaxis','shortaxis','R1','R2','R3','R4','R5','R6','height','relativeposition');
keydimension01 = [longaxis01 shortaxis01 R7 R8 R9 R10 R11 R12 L01];
save([name,'_5.mat'],'keydimension01');