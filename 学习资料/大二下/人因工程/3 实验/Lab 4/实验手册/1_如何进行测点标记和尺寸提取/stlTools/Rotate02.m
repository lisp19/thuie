%截取10mm往下的5mm内的数据点
upvalue02 = A1(3)-10+2.5;
downvalue02 = A1(3)-10-2.5;
temp08 = find(z>=downvalue02 & z<= upvalue02);
zcross = z(temp08);
crosssection02 = vf(ismember(vf(:,3),zcross),:);
x6 = crosssection02(:,1);
y6 = crosssection02(:,2);
z2 = crosssection02(:,3);
plot3(x6,y6,z2,'b.');%绘制三维投影图
crosssection02(:,3) = [];%投影到x，y
% x7 = crosssection02(:,1);
% y7 = crosssection02(:,2);
% plot(x7,y7,'g.');%绘制二维投影图
A4 = A1(1:2);
B4 = B1(1:2);
C4 = C1(1:2);
D4 = D1(1:2);
crosssection02(:,2) = crosssection02(:,2) - B4(2);
D4(:,2) = D4(:,2)-C4(2);
B4(:,2) = B4(:,2)-B4(2);
C4(:,2) = C4(:,2)-C4(2);
% x8 = crosssection02(:,1);
% y8 = crosssection02(:,2);
% plot(x8,y8,'g.');%绘制二维投影图

%将点划分为两组，获得yplus和yminus
temp09 = find(crosssection02(:,2) >= 0);
temp10 = find(crosssection02(:,2) < 0);
yplus01 = crosssection02(temp09,:);
yminus01 = crosssection02(temp10,:);

%分别对两组数据进行拟合，获得fx和gx
x9 = yplus01(:,1);
y9 = yplus01(:,2);
x10 = yminus01(:,1);
y10 = yminus01(:,2);
%plot(x6,y6,'g.');
%plot(x7,y7,'g.');
fx01 = polyfit(x9,y9,6);
gx01 = polyfit(x10,y10,6);

%求解所有的关键点
temp11 = roots(fx01);
B5 = [temp11(6),0];%拟合曲线与x轴交点，靠近大拇指；
C5 = [temp11(1),0];%拟合曲线与y轴交点，靠近小指；
halfaxis01 = (B5(1)+C5(1))/2;%二分之一横坐标
quarter01 = (B5(1)+halfaxis01)/2;%四分之一横坐标
threequarter01 = (halfaxis01+C5(1))/2;%四分之三横坐标
eighth01 = (B5(1)+quarter01)/2;%八分之一横坐标
seveneighth01 = (threequarter01+C5(1))/2;%八分之七横坐标
A5 = [halfaxis01,polyval(gx01,halfaxis01)];%手背端二分之一点
point10 = [eighth01, polyval(gx01,eighth01)];%手背端四分之一点，靠近大拇指
point11 = [quarter01, polyval(gx01,quarter01)];%手背端四分之一点，靠近大拇指
point12 = [threequarter01, polyval(gx01,threequarter01)];%手背端四分之三点，靠近小指
point13 = [seveneighth01, polyval(gx01,seveneighth01)];%手背端八分之七点，靠近小指
point14 = [eighth01, polyval(fx01,eighth01)];%手心端四分之一点，靠近大拇指
point15 = [quarter01, polyval(fx01,quarter01)];%手心端四分之一点，靠近大拇指
point16 = [threequarter01, polyval(fx01,threequarter01)];%手心端四分之三点，靠近小指
point17 = [seveneighth01, polyval(fx01,seveneighth01)];%手心端八分之七点，靠近小指
point18 = [halfaxis01,polyval(fx01,halfaxis01)];%手心端二分之一点
keypoint01 = [A1 B1 C1 D1 A2 B2 C2 A5 B5 C5 D2 point10 point11 point12 point13 point14 point15 point16 point17 point18];

%存储关键点
%save('D:\科研\01-华为手腕\01-手腕数据\关键点标记操作\Data\CP01_F010N_L.mat','A3','B3','C3','D2','point01','point02','point03','point04','point05','point06','point07','point08','point09');
save([name,'_4.mat'],'keypoint01');

%关键尺寸提取
longaxis01 = norm(B5-C5);
shortaxis01 = norm(A5-point18);
R7 = ThreePoint2Circle(B5,point10,point11);
R8 = ThreePoint2Circle(A5,point11,point12);
R9 = ThreePoint2Circle(C5,point12,point13);
R10 = ThreePoint2Circle(B5,point14,point15);
R11 = ThreePoint2Circle(point15,point16,point17);
R12 = ThreePoint2Circle(C5,point17,point18);
% height01 = 100;%尺骨茎突方法
% for i=1:size(crosssection02,1)
%     temp12 = crosssection02(i,:);
%     temp13 = norm(D2-temp12);
%     if temp13 < height01
%         height01 = temp13;
%     end
% end
%relativeposition = D2-A5;
xvalue01 = [B5(1):0.1:C5(1)];
yvalue3 = polyval(fx01,xvalue01);
yvalue4 = polyval(gx01,xvalue01);
d3 = sqrt(diff(xvalue01).^2+diff(yvalue3).^2);
d4 = sqrt(diff(xvalue01).^2+diff(yvalue4).^2);
L01 = sum(d3)+sum(d4);

%存储关键尺寸
%save ('D:\科研\01-华为手腕\01-手腕数据\关键点标记操作\Data\CD01_F010N_L.mat','longaxis','shortaxis','R1','R2','R3','R4','R5','R6','height','relativeposition');
keydimension01 = [longaxis01 shortaxis01 R7 R8 R9 R10 R11 R12 L01];
save([name,'_5.mat'],'keydimension01');