#### data clean ####
acs <- read.csv("C:/Users/JANE-/Desktop/统计/R相关/Lab3/acs_or.csv",header = T, sep = ",")
View(acs)

#### transform data ####
acs_new <- acs
acs_new$mode <- as.character(acs_new$mode)
acs_new$mode <- replace(acs_new$mode, which(acs_new$mode == "internet"), 1)
acs_new$mode <- reAplace(acs_new$mode, which(acs_new$mode != 1), 0)
acs_new$mode <- as.numeric(acs_new$mode)
View(acs_new)
acs_new$own <- as.character(acs_new$own)
acs_new$own <- replace(acs_new$own, which(acs_new$own == "Rented"), 0)
acs_new$own <- replace(acs_new$own, which(acs_new$own != 0), 1)
acs_new$own <- as.numeric(acs_new$own)

acs_new$language <- as.character(acs_new$language)
acs_new$language <- replace(acs_new$language, which(acs_new$language == "English only"), 1)
acs_new$language <- replace(acs_new$language, which(acs_new$language != 1), 0)
acs_new$language <- as.numeric(acs_new$language)

acs_new$internet <- as.character(acs_new$internet)
acs_new$internet <- replace(acs_new$internet, which(acs_new$internet == "Yes"), 1)
acs_new$internet <- replace(acs_new$internet, which(acs_new$internet != 1), 0)
acs_new$internet <- as.numeric(acs_new$internet)

View(acs_new)

#### simple linear regression ####
X <- acs_new$bedrooms
Y <- acs_new$electricity
model1 <- lm(Y~X)
summary(model1)
par(mfrow=c(2,2))#展示框分为两行两列，四幅图一起展现
plot(model1)
par(mfrow=c(1,1))
model1$coefficients#或model1$coefficients[2]
model1$residuals
plot(model1$residuals[1:10])
abline(h=0,lwd=2)
model1$fitted.values[1:2]

X <- acs_new$income_husband
Y <- acs_new$electricity
model2 <- lm(Y~X)
summary(model2)
par(mfrow=c(2,2))
plot(model2)




#### multiple linear regression ####
# linear relationship 
sp <- read.csv("C:/Users/JANE-/Desktop/统计/R相关/Lab3/SupervisorPerformance.csv",header = T, sep = ",")
View(sp)
par(mfrow=c(2,2))
plot(sp$X1,sp$Y,pch=16)
plot(sp$X3,sp$Y,pch=16)
plot(sp$X1, lm(sp$Y~sp$X1+sp$X3)$residuals + lm(sp$Y~sp$X1+sp$X3)$coefficient[2]*sp$X1)
plot(sp$X3, lm(sp$Y~sp$X1+sp$X3)$residuals + lm(sp$Y~sp$X1+sp$X3)$coefficient[3]*sp$X3)
pairs(sp[,2:8])

# correlation graphics
install.packages("corrgram")
library(corrgram)
corrgram(sp[,2:8],lower.panel = panel.conf)

# build a linear regression model
model3 <- lm(sp$Y~sp$X1+sp$X2+sp$X3+sp$X4+sp$X5+sp$X6)#summary里居然没有SSE……
summary(model3)
SSE <- sum((sp$Y - model3$fitted.values)^2)
SSR <- sum((model3$fitted.values - mean(sp$Y))^2)
SST <- SSE + SSR
R2 <- SSR/SST

# F-test
anova(model3)
model4 <- lm(sp$Y~sp$X1)
var.test(model3,model4)

model5 <- lm(sp$Y~sp$X2+sp$X5)
var.test(model3,model5)

# prediction 
X_NEW <- cbind(rnorm(10,50,10),rnorm(10,50,10),rnorm(10,50,10),
               rnorm(10,50,10),rnorm(10,50,10),rnorm(10,50,10))#随便生成了一组x
X_NEW <- as.data.frame(X_NEW)
colnames(X_NEW) <- colnames(sp)[-c(1:2)]
predict(model3, newdata = X_NEW, interval="predict") # does not work?

X_NEW <- cbind(rnorm(10,50,10),rnorm(10,50,10),rnorm(10,50,10),
               rnorm(10,50,10),rnorm(10,50,10),rnorm(10,50,10))
prediction <- model3$coefficients[1] + X_NEW%*%model3$coefficients[2:7]
prediction

#### analysis of residuals #### 
par(mfrow=c(2,2))
plot(model3)
par(mfrow=c(1,1))

#### log transformation ####
vit.dat <- read.csv("C:/Users/JANE-/Desktop/统计/R相关/Lab3/vit.dat.csv",header = T, sep = ",")
str(vit.dat)

model6 <- lm(weight ~ trtmt + block, vit.dat)
anova(model6)

vit.dat$resids <- residuals(model6)
vit.dat$preds <- predict(model6)
vit.dat$sq_preds <- vit.dat$preds^2
par(mfrow=c(1,1))
plot(resids ~ preds, data = vit.dat,
     xlab = "Predicted Values",
     ylab = "Residuals")

# check the problem 
View(vit.dat)

# log transformation
vit.dat$trans_weight <- log10(10*vit.dat$weight)
model7 <- lm(trans_weight ~ trtmt + block, vit.dat)
anova(model7)
vit.dat$trans_resids <- residuals(model7)
vit.dat$trans_preds <- predict(model7)
vit.dat$trans_sq_preds <- vit.dat$trans_preds^2
par(mfrow=c(1,1))
plot(trans_resids ~ trans_preds, data = vit.dat,
     xlab = "Predicted Values",
     ylab = "Residuals",pch=16)
abline(h=0)

#### power transformation ####
power.dat <- read.csv("C:/Users/JANE-/Desktop/统计/R相关/Lab3/power.dat.csv",header = T, sep = ",")
str(power.dat)
model8 <- lm(response ~ trtmt, power.dat)
summary(model8)
anova(model8)

#Generate residual and predicted values
power.dat$resids <- residuals(model8)
power.dat$preds <- predict(model8)
power.dat$sq_preds <- power.dat$preds^2
#Look at a plot of residual vs. predicted values
plot(resids ~ preds, data = power.dat, xlab = "Predicted Values",
     ylab = "Residuals",pch=16)
tapply(power.dat$response, INDEX=power.dat$trtmt, FUN = mean)
tapply(power.dat$response, INDEX=power.dat$trtmt, FUN = sd)

# finding the exponent for a power transformation 
library(MASS)
boxcox(model8, lambda = seq(-2,2,0.1))
boxcox(model8, lambda = seq(-1,0,0.1))
boxcox(model8, lambda = seq(-0.2,0,0.1))
# we choose power = -0.15
power.dat$trans_response<-(power.dat$response)^(-0.15)
#The ANOVA
model9<-lm(trans_response ~ trtmt, power.dat)
anova(model9)
anova(model8)
#Generate residual and predicted values
power.dat$trans_resids <- residuals(model9)
power.dat$trans_preds <- predict(model9)
#Look at a plot of residual vs. predicted values
plot(trans_resids ~ trans_preds, data = power.dat,
     xlab = "Predicted Values",
     ylab = "Residuals")
