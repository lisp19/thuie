#### review of basic operators ####
x <- rnorm(5, 7, 1.3)
x
#calculator
round(x)
#type
typeof(x)
as.character(x)
#vector
students <- c("Sean","Louisa","Frank","Farhad","Li") 
midterm <- c(80, 90, 93, 82, 95) 
students <- students[-4]
students
mean(midterm)
pmin(midterm,x)
sort(midterm)
order(midterm)
summary(midterm)
#names
students <- c("Sean","Louisa","Frank","Farhad","Li")
names(midterm) <- students 
midterm


#### data frame ####
survey <- read.csv("*C:\Users\JANE-\Desktop\统计\R相关\Lab1-1.csv",header = T, sep = ",")
View(survey)
#summary of data
class(survey)
head(survey,5)
str(survey)
summary(survey)
attributes(survey)
dim(survey)
nrow(survey)
ncol(survey)
plot(survey[["Program"]])
hist(survey$TVhours, col="yellow",labels=T,ylim=c(0,20))
#subsets of data
survey[3:5,c(1,5)]
survey[survey$Program=="MISM", ]
subset(survey, select=c("OperatingSystem", "TVhours"), 
       subset=(Program == "PPM" | Program == "Other") & Rexperience == "Basic competence")



#### data cleaning ####
survey.messy <- read.csv("C:/Users/JANE-/Desktop/统计/R相关/Lab1-2.csv",header = T, sep = ",")
View(survey.messy)
survey.messy$TVhours
str(survey.messy)
#as.numeric
tv.hours.messy <- survey.messy$TVhours
tv.hours.messy
as.numeric(tv.hours.messy)
#character -> numeric
as.character(tv.hours.messy)
as.numeric(as.character(tv.hours.messy))
typeof(as.numeric(as.character(tv.hours.messy)))
#missing data problem
tv.hours.strings <- as.character(tv.hours.messy)
tv.hours.strings
### Use gsub() to replace everything except digits and '.' with a blank ""
gsub("[^0-9.]", "", tv.hours.strings) 
tv.hours.clean <- as.numeric(gsub("[^0-9.]", "", tv.hours.strings))
tv.hours.clean
survey <- transform(survey.messy, TVhours = tv.hours.clean)
str(survey)
View(survey)

#another simpler approach
survey.messy <- read.csv("/Users/sue/Desktop/Lab1-2.csv", header=T, sep=",",stringsAsFactors=FALSE)
str(survey.messy)
survey <- transform(survey.messy, 
                    TVhours = as.numeric(gsub("[^0-9.]", "", TVhours)))
str(survey)
# Figure out which columns are coded as characters
chr.indexes <- sapply(survey, FUN = is.character)
chr.indexes
# Re-code all of the character columns to factors
survey[chr.indexes] <- lapply(survey[chr.indexes], FUN = as.factor)
str(survey)



#### apply functions ####
#lapply(), sapply()
lapply(survey, is.factor) # Returns a list
sapply(survey, FUN = is.factor) # Returns a vector with named elements
# another example
# "cars" is the data set from datasets package
cars <- cars
cars
head(cars)
apply(cars, 2, FUN=mean) # Data frames are arrays
lapply(cars, FUN=mean) # Data frames are also lists
sapply(cars, FUN=mean) # sapply() is just simplified lapply()
#tapply() example
library(MASS)
Cars93 <- Cars93
str(Cars93)
# Get a count table, data broken down by Origin and DriveTrain
table(Cars93$Origin, Cars93$DriveTrain)
# Calculate average MPG.City, broken down by Origin and Drivetrain
tapply(Cars93$MPG.city, INDEX = Cars93[c("Origin", "DriveTrain")], FUN=mean)

###Example: using tapply() to mimic table()
library(MASS)
# Get a count table, data broken down by Origin and DriveTrain
table(Cars93$Origin, Cars93$DriveTrain)
# This one may take a moment to figure out...
tapply(rep(1, nrow(Cars93)), INDEX = Cars93[c("Origin", "DriveTrain")], FUN=sum) 

#### with() ####
with(Cars93, table(Origin, Type))
any(Cars93$Origin == "non-USA" & Cars93$Type == "Large")
with(Cars93, any(Origin == "non-USA" & Type == "Large")) # Same effect!
with(Cars93, tapply(Horsepower, INDEX = list(Origin, Type), FUN=mean))


#### probaiblity plot ####
dnorm(0)
pnorm(0)
qnorm(0.5)
rnorm(10)
hist(rnorm(100))

mu <- 2
sigma2 <- 2^2
n1 <- 5
n2 <- 50
x <- seq(-2, 6, by = 0.01)
hx1 <- dnorm(x, mu, sqrt(sigma2/n1))
hx2 <- dnorm(x, mu, sqrt(sigma2/n2))
colors <- c("red","blue")
labels <- c("n=5", "n=50")

plot(x,hx2,type="l",lwd=3,col=colors[1],
     xlab="x value",ylab="Density",main="Comparison of Normal Distribution")
lines(x,hx1,lwd=3,col=colors[2])
legend("topright",title="Distributions",labels,lwd=3,col=colors)

lb <- -2
ub <- qnorm(0.25, mu, sqrt(sigma2/n1))
i <- x>=lb & x<=ub
polygon(c(lb,x[i],ub),c(0,hx1[i],0),col="blue")


#### ggplot2() (harvard tutorial) ####
housing <- read.csv("/Users/sue/Desktop/Lab1-3.csv",header = T, sep=",")
head(housing[1:5])
#### base graphics vs. ggplot2
#histogram for home value
hist(housing$Home.Value)
library(ggplot2)
ggplot(housing, aes(x = Home.Value)) +
  geom_histogram()
#home value ~ data
plot(Home.Value ~ Date,
     data=subset(housing, State == "MA"))
points(Home.Value ~ Date, col="red",
       data=subset(housing, State == "TX"))
legend(1975, 400000,
       c("MA", "TX"), title="State",
       col=c("black", "red"),
       pch=c(1, 1))
ggplot(subset(housing, State %in% c("MA", "TX")),
       aes(x=Date,
           y=Home.Value,
           color=State))+
  geom_point()

#### aes() and geom_() functions
#geom_point
hp2001Q1 <- subset(housing, Date == 2001.25) 
ggplot(hp2001Q1,
       aes(y = Structure.Cost, x = Land.Value)) +
  geom_point()
ggplot(hp2001Q1,
       aes(y = Structure.Cost, x = log(Land.Value))) +
  geom_point()
#adding predictive line
hp2001Q1$pred.SC <- predict(lm(Structure.Cost ~ log(Land.Value), data = hp2001Q1))
p1 <- ggplot(hp2001Q1, aes(x = log(Land.Value), y = Structure.Cost))
p1
p1 + geom_point(aes(color = Home.Value)) +
  geom_line(aes(y = pred.SC))
#smoother
p1 +
  geom_point(aes(color = Home.Value)) +
  geom_smooth()
#using name
p1 + 
  geom_text(aes(label=State), size = 3)
#name as marks by using ggrepel
#install.packages("ggrepel")
library("ggrepel")
p1 + 
  geom_point() + 
  geom_text_repel(aes(label=State), size = 3)
#adjust aes function
p1 +
  geom_point(aes(size = 2),# incorrect! 2 is not a variable
             color="red") # this is fine -- all points red
p1 +
  geom_point(aes(color=Home.Value, shape = region))

#### statistical transformation 
#adjust geom_ function
p2 <- ggplot(housing, aes(x = Home.Value))
p2 + geom_histogram()
p2 + geom_histogram(stat = "bin", binwidth=4000)
#statistical trasformation
housing.sum <- aggregate(housing["Home.Value"], housing["State"], FUN=mean)
rbind(head(housing.sum), tail(housing.sum))
#adjust default parameters
ggplot(housing.sum, aes(x=State, y=Home.Value)) + 
  geom_bar()
ggplot(housing.sum, aes(x=State, y=Home.Value)) + 
  geom_bar(stat="identity")

#### scales 
p3 <- ggplot(housing,
             aes(x = State,
                 y = Home.Price.Index)) + 
  theme(legend.position="top",
        axis.text=element_text(size = 6))
p4 <- p3 + geom_point(aes(color = Date),
                    alpha = 0.5,
                    size = 1.5,
                    position = position_jitter(width = 0.25, height = 0))
p4
#change the color scale
p4 + scale_x_discrete(name="State Abbreviation") +
  scale_color_continuous(name="Date",
                         breaks = c(1976, 1994, 2013),
                         labels = c("'76", "'94", "'13"))
#change the color
p4 +
  scale_x_discrete(name="State Abbreviation") +
  scale_color_continuous(name="",
                         breaks = c(1976, 1994, 2013),
                         labels = c("'76", "'94", "'13"),
                         low = "blue", high = "red")
#install.packages("scales")
library(scales)
p4 +
  scale_color_continuous(name="",
                         breaks = c(1976, 1994, 2013),
                         labels = c("'76", "'94", "'13"),
                         low = muted("blue"), high = muted("red"))

#### faceting 
p5 <- ggplot(housing, aes(x = Date, y = Home.Value))
p5 + geom_line(aes(color = State))
p5 + geom_line() +
  facet_wrap(~State, ncol = 10)

#### themes
p5 <- p5 + geom_line() +
  facet_wrap(~State, ncol = 10)
p5 + theme_linedraw()
p5 + theme_light()
# creating a new theme
theme_new <- theme_bw() +
  theme(plot.background = element_rect(size = 1, color = "blue", fill = "black"),
        text=element_text(size = 12),
        axis.text.y = element_text(colour = "purple"),
        axis.text.x = element_text(colour = "red"),
        panel.background = element_rect(fill = "pink"),
        strip.background = element_rect(fill = muted("orange")))
p5 + theme_new










#### ggplot2 another example ####
#download data
library(ggplot2)
nmmaps <- read.csv("/Users/sue/Desktop/Lab1-4.csv", as.is=T)
nmmaps$date <- as.Date(nmmaps$date)
nmmaps <- nmmaps[nmmaps$date>as.Date("1996-12-31"),]
nmmaps$year <- substring(nmmaps$date,1,4)
head(nmmaps)
#a default plot
g <- ggplot(nmmaps, aes(date, temp))+geom_point(color="firebrick")
g
#add a title
g <- g+ggtitle('Temperature')
#or 
#g <- g+labs(title='Temperature')
g
#make title bold and add a little space at the baseline
g+theme(plot.title = element_text(size=20, face="bold", 
                                  margin = margin(10, 0, 10, 0)))
#use a non-traditional font in your title
library(extrafont)
g+theme(plot.title = element_text(size=30,lineheight=.8, 
                                  vjust=1,family="Bauhaus 93"))
#Change spacing in multi-line text (lineheight)
g<-g+ggtitle("This is a longer\ntitle than expected")
g+theme(plot.title = element_text(size=20, face="bold", vjust=1, lineheight=0.6))
##Working with axes
#Add x and y axis labels (labs(), xlab())
g<-g+labs(x="Date", y=expression(paste("Temperature ( ", degree ~ F, " )")), title="Temperature")
g
#Get rid of axis ticks and tick text (theme(), axis.ticks.y)
g + theme(axis.ticks.y = element_blank(),axis.text.y = element_blank())
#Change size of and rotate tick text (axis.text.x)
g + theme(axis.text.x=element_text(angle=50, size=20, vjust=0.5))
#Move the labels away from the plot (and add color) (theme(), axis.title.x)
g + theme(
  axis.title.x = element_text(color="forestgreen", vjust=-0.35),
  axis.title.y = element_text(color="cadetblue" , vjust=0.35)   
)
#If you want the axes to be the same (coord_equal())
ggplot(nmmaps, aes(temp, temp+rnorm(nrow(nmmaps), sd=20)))+geom_point()+
  xlim(c(0,150))+ylim(c(0,150))+
  coord_equal()
#Use a function to alter labels (label=function(x){})
ggplot(nmmaps, aes(date, temp))+
  geom_point(color="grey")+
  labs(x="Month", y="Temp")+
  scale_y_continuous(label=function(x){return(paste("My value is", x, "degrees"))})
##Working with the legend
g<-ggplot(nmmaps, aes(date, temp, color=factor(season)))+geom_point()
g
#Turn off the legend title (legend.title)
g+theme(legend.title=element_blank())
#Change the styling of the legend title (legend.title)
g+theme(legend.title = element_text(colour="chocolate", size=16, face="bold"))
#Change the title of the legend (name)
g+theme(legend.title = element_text(colour="chocolate", size=16, face="bold"))+
  scale_color_discrete(name="This color is\ncalled chocolate!?")
#Change the background boxes in the legend (legend.key)
g+theme(legend.key=element_rect(fill='pink'))
#Change the size of the symbols in the legend only (guides(), guide_legend)
g+guides(colour = guide_legend(override.aes = list(size=4)))
#Leave a layer off the legend (show_guide)
g+geom_text(data=nmmaps, aes(date, temp, label=round(temp)), size=4)
#You can use show_guide=FALSE to turn a layer off in the legend. Useful!
g+geom_text(data=nmmaps, aes(date, temp, label=round(temp), size=4), show_guide=FALSE)
#Manually adding legend items (guides(), override.aes)
ggplot(nmmaps, aes(x=date, y=o3))+geom_line(color="grey")+geom_point(color="red")
ggplot(nmmaps, aes(x=date, y=o3))+geom_line(aes(color="Important line"))+
  geom_point(aes(color="My points"))
ggplot(nmmaps, aes(x=date, y=o3))+geom_line(aes(color="Important line"))+
  geom_point(aes(color="Point values"))+
  scale_colour_manual(name='', values=c('Important line'='grey', 'Point values'='red'))
ggplot(nmmaps, aes(x=date, y=o3))+geom_line(aes(color="Important line"))+
  geom_point(aes(color="Point values"))+
  scale_colour_manual(name='', values=c('Important line'='grey', 'Point values'='red'), guide='legend') +
  guides(colour = guide_legend(override.aes = list(linetype=c(1,0)
                                                   , shape=c(NA, 16))))
##Working with the background colors
#Change the panel color (panel.background)
ggplot(nmmaps, aes(date, temp))+geom_point(color="firebrick")+
  theme(panel.background = element_rect(fill = 'grey75'))
#Change the grid lines (panel.grid.major)
ggplot(nmmaps, aes(date, temp))+geom_point(color="firebrick")+
  theme(panel.background = element_rect(fill = 'grey75'),
        panel.grid.major = element_line(colour = "orange", size=2),
        panel.grid.minor = element_line(colour = "blue"))
#Change the plot background (not the panel) color (plot.background)
ggplot(nmmaps, aes(date, temp))+geom_point(color="firebrick")+
  theme(plot.background = element_rect(fill = 'grey'))
##Working with margins
#Changing the plot margin (plot.margin)
# the default
ggplot(nmmaps, aes(date, temp))+
  geom_point(color="darkorange3")+
  labs(x="Month", y="Temp")+
  theme(plot.background=element_rect(fill="darkseagreen"))
library(grid)
ggplot(nmmaps, aes(date, temp))+
  geom_point(color="darkorange3")+
  labs(x="Month", y="Temp")+
  theme(plot.background=element_rect(fill="darkseagreen"),
        plot.margin = unit(c(1, 6, 1, 6), "cm")) #top, right, bottom, left
#Creating multi-panel plots
ggplot(nmmaps, aes(date,temp))+geom_point(color="aquamarine4")+facet_wrap(~year, nrow=1)
#Create a matrix of plots based on one variable (facet_wrap())
ggplot(nmmaps, aes(date,temp))+geom_point(color="chartreuse4")+
  facet_wrap(~year, ncol=2)
#Allow scales to roam free (scales)
ggplot(nmmaps, aes(date,temp))+geom_point(color="chartreuse4")+
  facet_wrap(~year, ncol=2, scales="free")
#Create a grid of plots using two variables (facet_grid())
ggplot(nmmaps, aes(date,temp))+geom_point(color="darkgoldenrod4")+
  facet_grid(year~season)
#Put two (potentially unrelated) plots side by side (pushViewport(), grid.arrange())
myplot1<-ggplot(nmmaps, aes(date, temp))+geom_point(color="firebrick")
myplot2<-ggplot(nmmaps, aes(temp, o3))+geom_point(color="olivedrab")
library(grid)
pushViewport(viewport(layout = grid.layout(1, 2)))
print(myplot1, vp = viewport(layout.pos.row = 1, layout.pos.col = 1))
print(myplot2, vp = viewport(layout.pos.row = 1, layout.pos.col = 2))
# alternative, a little easier
library(gridExtra)
grid.arrange(myplot1, myplot2, ncol=2)
##Working with themes
#Use a new theme (theme_XX())
library(ggthemes)
ggplot(nmmaps, aes(date, temp, color=factor(season)))+
  geom_point()+ggtitle("This plot looks a lot different from the default")+
  theme_economist()+scale_colour_economist()
#Change the size of all plot text elements
theme_set(theme_gray(base_size = 30))
ggplot(nmmaps, aes(x=date, y=o3))+geom_point(color="red")
#Tip on creating a custom theme
#If you want to change the theme for an entire session you can use theme_set as in theme_set(theme_bw()). The default is called theme_gray. If you wanted to create your own custom theme, you could extract the code directly from the gray theme and modify. Note that the rel() function change the sizes relative to the base_size.
theme_gray
function (base_size = 12, base_family = "") 
{
  theme(
    line = element_line(colour = "black", size = 0.5, linetype = 1, lineend = "butt"), 
    rect = element_rect(fill = "white", colour = "black", size = 0.5, linetype = 1), 
    text = element_text(family = base_family, face = "plain", colour = "black", size = base_size, hjust = 0.5, vjust = 0.5, angle = 0, lineheight = 0.9), 
    
    axis.text = element_text(size = rel(0.8), colour = "grey50"), 
    strip.text = element_text(size = rel(0.8)), 
    axis.line = element_blank(), 
    axis.text.x = element_text(vjust = 1), 
    axis.text.y = element_text(hjust = 1), 
    axis.ticks = element_line(colour = "grey50"), 
    axis.title.x = element_text(), 
    axis.title.y = element_text(angle = 90), 
    axis.ticks.length = unit(0.15, "cm"), 
    axis.ticks.margin = unit(0.1, "cm"), 
    
    legend.background = element_rect(colour = NA), 
    legend.margin = unit(0.2, "cm"), 
    legend.key = element_rect(fill = "grey95", colour = "white"), 
    legend.key.size = unit(1.2, "lines"), 
    legend.key.height = NULL, 
    legend.key.width = NULL, 
    legend.text = element_text(size = rel(0.8)), 
    legend.text.align = NULL, 
    legend.title = element_text(size = rel(0.8), face = "bold", hjust = 0), 
    legend.title.align = NULL, 
    legend.position = "right", 
    legend.direction = NULL, 
    legend.justification = "center", 
    legend.box = NULL, 
    
    panel.background = element_rect(fill = "grey90", colour = NA), 
    panel.border = element_blank(), 
    panel.grid.major = element_line(colour = "white"), 
    panel.grid.minor = element_line(colour = "grey95", size = 0.25), 
    panel.margin = unit(0.25, "lines"), 
    panel.margin.x = NULL, 
    panel.margin.y = NULL, 
    
    strip.background = element_rect(fill = "grey80", colour = NA), 
    strip.text.x = element_text(), 
    strip.text.y = element_text(angle = -90), 
    
    plot.background = element_rect(colour = "white"), 
    plot.title = element_text(size = rel(1.2)), 
    plot.margin = unit(c(1, 1, 0.5, 0.5), "lines"), complete = TRUE)
}
##Working with colors
#Categorical variables: manually select the colors (scale_color_manual())
ggplot(nmmaps, aes(date, temp, color=factor(season)))+
  geom_point() + 
  scale_color_manual(values=c("dodgerblue4", "darkolivegreen4",
                              "darkorchid3", "goldenrod1"))
#Categorical variables: try a built-in palette (based on colorbrewer2.org) (scale_color_brewer()):
ggplot(nmmaps, aes(date, temp, color=factor(season)))+
  geom_point() + 
  scale_color_brewer(palette="Set1")
library(ggthemes)
ggplot(nmmaps, aes(date, temp, color=factor(season)))+
  geom_point() + 
  scale_colour_tableau()
#Color choice with continuous variables (scale_color_gradient(), scale_color_gradient2())
ggplot(nmmaps, aes(date, temp, color=o3))+geom_point()
#Manually change the low and high colors (sequential color scheme):
ggplot(nmmaps, aes(date, temp, color=o3))+geom_point()+
  scale_color_gradient(low="darkkhaki", high="darkgreen")
mid<-mean(nmmaps$o3)
ggplot(nmmaps, aes(date, temp, color=o3))+geom_point()+
  scale_color_gradient2(midpoint=mid,
                        low="blue", mid="white", high="red" )
##Working with annotation
#Add text annotation in the top-right, top-left etc. (annotation_custom() and textGrob())
library(grid)
my_grob = grobTree(textGrob("This text stays in place!", x=0.1,  y=0.95, hjust=0,
                            gp=gpar(col="blue", fontsize=15, fontface="italic")))

ggplot(nmmaps, aes(temp, o3))+geom_point(color="firebrick")+
  annotation_custom(my_grob)
library(grid)
my_grob = grobTree(textGrob("This text stays in place!", x=0.1,  y=0.95, hjust=0,
                            gp=gpar(col="blue", fontsize=12, fontface="italic")))
ggplot(nmmaps, aes(temp, o3))+geom_point(color="firebrick")+facet_wrap(~season, scales="free")+
  annotation_custom(my_grob)
##Working with coordinates
#Flip a plot on it???s side (coord_flip())
ggplot(nmmaps, aes(x=season, y=o3))+geom_boxplot(fill="chartreuse4")+coord_flip()
#Alternatives to the box plot (geom_jitter() and geom_violin())
#Start with a box plot
g<-ggplot(nmmaps, aes(x=season, y=o3))
g+geom_boxplot(fill="darkseagreen4")
#Alternative to a box plot: plot of points
g+geom_point()
#Alternative to a box plot: jitter the points (geom_jitter())
g+geom_jitter(alpha=0.5, aes(color=season),position = position_jitter(width = .2))
#Alternative to a box plot: violin plot perhaps (geom_violin())
g+geom_violin(alpha=0.5, color="gray")
g+geom_violin(alpha=0.5, color="gray")+geom_jitter(alpha=0.5, aes(color=season),
                                                   position = position_jitter(width = 0.1))+coord_flip()
#Add a ribbon to your plot (geom_ribbon())
# add a filter
nmmaps$o3run<-as.numeric(filter(nmmaps$o3, rep(1/30,30), sides=2))
ggplot(nmmaps, aes(date, o3run))+geom_line(color="lightpink4", lwd=1)
# add a filter
ggplot(nmmaps, aes(date, o3run))+geom_ribbon(aes(ymin=0, ymax=o3run), fill="lightpink3", color="lightpink3")+
  geom_line(color="lightpink4", lwd=1)
nmmaps$mino3<-nmmaps$o3run-sd(nmmaps$o3run, na.rm=T)
nmmaps$maxo3<-nmmaps$o3run+sd(nmmaps$o3run, na.rm=T)
ggplot(nmmaps, aes(date, o3run))+geom_ribbon(aes(ymin=mino3, ymax=maxo3), fill="steelblue2", color="steelblue2")+
  geom_line(color="steelblue4", lwd=1)
#Create a tiled correlation plot (geom_tile())
#careful, I'm sorting the field names so that the ordering in the final plot is correct
thecor<-round(cor(nmmaps[,sort(c("death", "temp", "dewpoint", "pm10", "o3"))], method="pearson", use="pairwise.complete.obs"),2)
thecor[lower.tri(thecor)]<-NA
thecor
library(reshape2)
thecor<-melt(thecor)
thecor$Var1<-as.character(thecor$Var1)
thecor$Var2<-as.character(thecor$Var2)
thecor<-na.omit(thecor)
head(thecor)
ggplot(thecor, aes(Var2, Var1))+
  geom_tile(data=thecor, aes(fill=value), color="white")+
  scale_fill_gradient2(low="blue", high="red", mid="white", 
                       midpoint=0, limit=c(-1,1),name="Correlation\n(Pearson)")+
  theme(axis.text.x = element_text(angle=45, vjust=1, size=11, hjust=1))+
  coord_equal()
##Working with smooths
#Default ??? adding LOESS or GAM (stat_smooth())
ggplot(nmmaps, aes(date, temp))+geom_point(color="firebrick")+
  stat_smooth()
#Specifying the formula (stat_smooth(formula=))
ggplot(nmmaps, aes(date, temp))+
  geom_point(color="grey")+
  stat_smooth(method="gam", formula=y~s(x,k=10), col="darkolivegreen2", se=FALSE, size=1)+
  stat_smooth(method="gam", formula=y~s(x,k=30), col="red", se=FALSE, size=1)+
  stat_smooth(method="gam", formula=y~s(x,k=500), col="dodgerblue4", se=FALSE, size=1)
#Adding a linear fit (stat_smooth(method="lm"))
ggplot(nmmaps, aes(temp, death))+geom_point(color="firebrick")+
  stat_smooth(method="lm", se=FALSE)
lmTemp<-lm(death~temp, data=nmmaps)
ggplot(nmmaps, aes(temp, death))+geom_point(col="firebrick")+
  geom_abline(intercept=lmTemp$coef[1], slope=lmTemp$coef[2])
