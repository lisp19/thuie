## OR Homework 5

工62  赵芃  2016010863



### Problem 1

Use the same notations as in the lecture.
$$
\max -2x_b-4x_f-5.2x_c+60y_d+40y_t+10y_c\\
\text{s.t.}\quad y_d\le165\\
y_t\le130\\
y_c\le310\\
-x_b+8y_d+6y_t+y_c\le0\\
-x_f+4y_d+2y_t+1.5y_c\le0\\
-x_c+2y_d+1.5y_t+0.5y_c\le0\\
x_b,x_f,x_c,y_d,y_t,y_c\ge0
$$

```
Objective value:                              4490.000
                                Variable           Value        Reduced Cost
                                      XB        2100.000            0.000000
                                      XF        920.0000            0.000000
                                      XC        525.0000            0.000000
                                      YD        165.0000            0.000000
                                      YT        130.0000            0.000000
                                      YC        0.000000           0.6000000
```

The solution is to produce 165 desks, 130 tables and 0 chairs. Total profit is 4490.



### Problem 2

#### (1) low scenario

$$
\max -2x_b-4x_f-5.2x_c+60y_d+40y_t+10y_c\\
\text{s.t.}\quad y_d\le100\\
y_t\le50\\
y_c\le200\\
-x_b+8y_d+6y_t+y_c\le0\\
-x_f+4y_d+2y_t+1.5y_c\le0\\
-x_c+2y_d+1.5y_t+0.5y_c\le0\\
x_b,x_f,x_c,y_d,y_t,y_c\ge0
$$

```
Objective value:                              2370.000
                                Variable           Value        Reduced Cost
                                      XB        1100.000            0.000000
                                      XF        500.0000            0.000000
                                      XC        275.0000            0.000000
                                      YD        100.0000            0.000000
                                      YT        50.00000            0.000000
                                      YC        0.000000           0.6000000
```

The solution is to produce 100 desks, 50 tables and 0 chairs. Total profit is 2370.

#### (2) most likely scenario

$$
\max -2x_b-4x_f-5.2x_c+60y_d+40y_t+10y_c\\
\text{s.t.}\quad y_d\le150\\
y_t\le100\\
y_c\le250\\
-x_b+8y_d+6y_t+y_c\le0\\
-x_f+4y_d+2y_t+1.5y_c\le0\\
-x_c+2y_d+1.5y_t+0.5y_c\le0\\
x_b,x_f,x_c,y_d,y_t,y_c\ge0
$$

```
Objective value:                              3860.000
                                Variable           Value        Reduced Cost
                                      XB        1800.000            0.000000
                                      XF        800.0000            0.000000
                                      XC        450.0000            0.000000
                                      YD        150.0000            0.000000
                                      YT        100.0000            0.000000
                                      YC        0.000000           0.6000000
```

The solution is to produce 150 desks, 100 tables and 0 chairs. Total profit is 3860.

#### (3) high scenario

$$
\max -2x_b-4x_f-5.2x_c+60y_d+40y_t+10y_c\\
\text{s.t.}\quad y_d\le250\\
y_t\le250\\
y_c\le500\\
-x_b+8y_d+6y_t+y_c\le0\\
-x_f+4y_d+2y_t+1.5y_c\le0\\
-x_c+2y_d+1.5y_t+0.5y_c\le0\\
x_b,x_f,x_c,y_d,y_t,y_c\ge0
$$

```
Objective value:                              7450.000
                                Variable           Value        Reduced Cost
                                      XB        3500.000            0.000000
                                      XF        1500.000            0.000000
                                      XC        875.0000            0.000000
                                      YD        250.0000            0.000000
                                      YT        250.0000            0.000000
                                      YC        0.000000           0.6000000
```

The solution is to produce 250 desks, 250 tables and 0 chairs. Total profit is 7450.



### Problem 3

$$
\max -2x_b-4x_f-5.2x_c+0.3(60y_{ld}+40y_{lt}+10y_{lc})+\\
0.4(60y_{md}+40y_{mt}+10y_{mc})+0.3(60y_{hd}+40y_{ht}+10y_{hc})\\
\text{s.t.}\quad y_{ld}\le100\\
y_{lt}\le50\\
y_{lc}\le200\\
y_{md}\le150\\
y_{mt}\le100\\
y_{mc}\le250\\
y_{hd}\le250\\
y_{ht}\le250\\
y_{hc}\le500\\
-x_b+8y_{wd}+6y_{wt}+y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
-x_f+4y_{wd}+2y_{wt}+1.5y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
-x_c+2y_{wd}+1.5y_{wt}+0.5y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
x_b,x_f,x_c\ge0\\
y_{wd},y_{wt},y_{wc}\ge0,\ \forall w\in\{l,m,h\}
$$

```
Objective value:                              2960.000
                                Variable           Value        Reduced Cost
                                      XB        1800.000            0.000000
                                      XF        800.0000            0.000000
                                      XC        450.0000            0.000000
                                     YLD        100.0000            0.000000
                                     YLT        50.00000            0.000000
                                     YLC        200.0000            0.000000
                                     YMD        150.0000            0.000000
                                     YMT        100.0000            0.000000
                                     YMC        0.000000            0.000000
                                     YHD        150.0000            0.000000
                                     YHT        100.0000            0.000000
                                     YHC        0.000000            2.050000
```

The optimal solution is shown below:

<img src="p1.png" style="zoom:70%"/>



### Problem 4

#### (1) MVP

$$
\max -2*2100-4*920-5.2*525+0.3(60y_{ld}+40y_{lt}+10y_{lc})+\\
0.4(60y_{md}+40y_{mt}+10y_{mc})+0.3(60y_{hd}+40y_{ht}+10y_{hc})\\
\text{s.t.}\quad y_{ld}\le100\\
y_{lt}\le50\\
y_{lc}\le200\\
y_{md}\le150\\
y_{mt}\le100\\
y_{mc}\le250\\
y_{hd}\le250\\
y_{ht}\le250\\
y_{hc}\le500\\
-2100+8y_{wd}+6y_{wt}+y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
-920+4y_{wd}+2y_{wt}+1.5y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
-525+2y_{wd}+1.5y_{wt}+0.5y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
y_{wd},y_{wt},y_{wc}\ge0,\ \forall w\in\{l,m,h\}
$$

```
Objective value:                              2440.000
                                Variable           Value        Reduced Cost
                                     YLD        100.0000            0.000000
                                     YLT        50.00000            0.000000
                                     YLC        200.0000            0.000000
                                     YMD        150.0000            0.000000
                                     YMT        100.0000            0.000000
                                     YMC        80.00000            0.000000
                                     YHD        165.0000            0.000000
                                     YHT        130.0000            0.000000
                                     YHC        0.000000            2.250000
```

#### (2) low scenario

$$
\max -2*1100-4*500-5.2*275+0.3(60y_{ld}+40y_{lt}+10y_{lc})+\\
0.4(60y_{md}+40y_{mt}+10y_{mc})+0.3(60y_{hd}+40y_{ht}+10y_{hc})\\
\text{s.t.}\quad y_{ld}\le100\\
y_{lt}\le50\\
y_{lc}\le200\\
y_{md}\le150\\
y_{mt}\le100\\
y_{mc}\le250\\
y_{hd}\le250\\
y_{ht}\le250\\
y_{hc}\le500\\
-1100+8y_{wd}+6y_{wt}+y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
-500+4y_{wd}+2y_{wt}+1.5y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
-275+2y_{wd}+1.5y_{wt}+0.5y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
y_{wd},y_{wt},y_{wc}\ge0,\ \forall w\in\{l,m,h\}
$$

```
Objective value:                              2370.000
                                Variable           Value        Reduced Cost
                                     YLD        100.0000            0.000000
                                     YLT        50.00000            0.000000
                                     YLC        0.000000            0.000000
                                     YMD        100.0000            0.000000
                                     YMT        50.00000            0.000000
                                     YMC        0.000000            3.000000
                                     YHD        100.0000            0.000000
                                     YHT        50.00000            0.000000
                                     YHC        0.000000            2.250000
```

#### (3) most likely scenario

$$
\max -2*1800-4*800-5.2*450+0.3(60y_{ld}+40y_{lt}+10y_{lc})+\\
0.4(60y_{md}+40y_{mt}+10y_{mc})+0.3(60y_{hd}+40y_{ht}+10y_{hc})\\
\text{s.t.}\quad y_{ld}\le100\\
y_{lt}\le50\\
y_{lc}\le200\\
y_{md}\le150\\
y_{mt}\le100\\
y_{mc}\le250\\
y_{hd}\le250\\
y_{ht}\le250\\
y_{hc}\le500\\
-1800+8y_{wd}+6y_{wt}+y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
-800+4y_{wd}+2y_{wt}+1.5y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
-450+2y_{wd}+1.5y_{wt}+0.5y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
y_{wd},y_{wt},y_{wc}\ge0,\ \forall w\in\{l,m,h\}
$$

```
Objective value:                              2960.000
                                Variable           Value        Reduced Cost
                                     YLD        100.0000            0.000000
                                     YLT        50.00000            0.000000
                                     YLC        200.0000            0.000000
                                     YMD        150.0000            0.000000
                                     YMT        100.0000            0.000000
                                     YMC        0.000000            0.000000
                                     YHD        150.0000            0.000000
                                     YHT        100.0000            0.000000
                                     YHC        0.000000            2.250000
```

#### (4) high scenario

$$
\max -2*3500-4*1500-5.2*875+0.3(60y_{ld}+40y_{lt}+10y_{lc})+\\
0.4(60y_{md}+40y_{mt}+10y_{mc})+0.3(60y_{hd}+40y_{ht}+10y_{hc})\\
\text{s.t.}\quad y_{ld}\le100\\
y_{lt}\le50\\
y_{lc}\le200\\
y_{md}\le150\\
y_{mt}\le100\\
y_{mc}\le250\\
y_{hd}\le250\\
y_{ht}\le250\\
y_{hc}\le500\\
-3500+8y_{wd}+6y_{wt}+y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
-1500+4y_{wd}+2y_{wt}+1.5y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
-875+2y_{wd}+1.5y_{wt}+0.5y_{wc}\le0,\ \forall w\in\{l,m,h\}\\
y_{wd},y_{wt},y_{wc}\ge0,\ \forall w\in\{l,m,h\}
$$

```
Objective value:                             -850.0000
                                Variable           Value        Reduced Cost
                                     YLD        100.0000            0.000000
                                     YLT        50.00000            0.000000
                                     YLC        200.0000            0.000000
                                     YMD        150.0000            0.000000
                                     YMT        100.0000            0.000000
                                     YMC        250.0000            0.000000
                                     YHD        250.0000            0.000000
                                     YHT        250.0000            0.000000
                                     YHC        0.000000            0.000000
```

#### (5) summary

<img src="p2.png" style="zoom:70%"/>



### Problem 5

$$
\text{Upper bound:}\quad c\bar{x}+h(\bar{x},E[\tilde{w}])=4490\\
\text{Lower bound:}\quad c\bar{x}+E[h(\bar{x},\tilde{w})]=2440
$$



### Problem 6

$$
EVPI=E[\hat{f_w}(\hat{x_w})]-f(x^*)=0.3*2370+0.4*3860+0.3*7450-2960=1530\\
VSS=f(x^*)-f(\bar{x})=2960-2440=520
$$

