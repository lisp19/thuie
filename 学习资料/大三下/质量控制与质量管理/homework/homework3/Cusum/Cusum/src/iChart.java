import java.math.BigDecimal;


public class iChart {

	/**
	 * @param args
	 */
	static double c;
	static int TR;
	static double H;
	static double ARL_0;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		iChart_0();
		iChart_1();
		}

	public static  void iChart_0(){
		ARL_0=1;
		TR = 2000000;
		c = 0;
		System.out.println("mean shift = 0");
		for(H=2.75;ARL_0<197;H=H+0.005){
			double Sumrl = 0;
			for(int j = 0; j < TR; j++){
			   c = Norm_rand(0);
			   if(Math.abs(c)> H){
				   Sumrl++;
				   };		   
			}
			if(Sumrl!=0){
				ARL_0 = TR/Sumrl;}
				else ARL_0=TR;
		}	
		System.out.println("ARL = "+new BigDecimal(ARL_0).setScale(5, BigDecimal.ROUND_HALF_UP));
		System.out.println("H ="+new BigDecimal(H).setScale(3, BigDecimal.ROUND_HALF_UP));
	}
	
	public static  void iChart_1(){
		TR = 2000000;
		c = 0;
		for(double i=0;i<5;i=i+0.1){
			System.out.println("mean shift = "+new BigDecimal(i).setScale(1, BigDecimal.ROUND_HALF_UP));
			double ARL;
			double Sumrl = 0;
			for(int j = 0; j < TR; j++){
			   c = Norm_rand(i);
			   if(Math.abs(c)> H){
				   Sumrl++;
				   };		   
			}
			if(Sumrl!=0){
				ARL = TR/Sumrl;}
				else ARL=TR;	
			System.out.println("ARL = "+new BigDecimal(ARL).setScale(5, BigDecimal.ROUND_HALF_UP));
		}
	}


	public static double Norm_rand(double miu){
		double N = 12;
		double x=0,temp=N;
		for(int i=0;i<N;i++)
			x=x+(Math.random());
		x=(x-temp/2)/(Math.sqrt(temp/12));
		return x+miu;
		}
}
