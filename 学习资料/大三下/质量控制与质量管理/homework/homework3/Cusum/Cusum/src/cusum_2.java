import java.math.BigDecimal;


public class cusum_2 {

	/**
	 * @param args
	 */
	static double c,d,x;
	static int TR;
	static double H;
	static double ARL_0;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("When K = 1, we have:");
		cusum_k1_0();
		cusum_k1_1();
		System.out.println("-------------------------");
		System.out.println("When K = 2, we have:");
		cusum_k2_0();
		cusum_k2_1();
		}
	
	
	public static  void cusum_k1_0(){
		ARL_0=1;
		TR = 2000000;
		c = 0;
		d = 0;
		x = 0;
		System.out.println("mean shift = 0");
		for(H=2.15;ARL_0<197;H=H+0.005){
			double Sumrl = 0;
			for(int j = 0; j < TR; j++){
			   x = Norm_rand(0);
			   c=Math.max(0, x-1+c);
			   d=Math.max(0, -1-x+d);
			   if(c > H||d > H){
				   Sumrl++;
				   c=0;
				   d=0;
				   };		   
			}
			if(Sumrl!=0){
				ARL_0 = TR/Sumrl;}
				else ARL_0=TR;
		}	
		System.out.println("ARL = "+new BigDecimal(ARL_0).setScale(5, BigDecimal.ROUND_HALF_UP));
		System.out.println("H ="+new BigDecimal(H).setScale(3, BigDecimal.ROUND_HALF_UP));
	}
	
	public static  void cusum_k1_1(){
		TR = 2000000;
		c = 0;
		d = 0;
		x = 0;
		for(double i=0;i<5;i=i+0.1){
			System.out.println("mean shift = "+new BigDecimal(i).setScale(1, BigDecimal.ROUND_HALF_UP));
			double ARL;
			double Sumrl = 0;
			for(int j = 0; j < TR; j++){
				 x = Norm_rand(i);
				   c=Math.max(0, x-1+c);
				   d=Math.max(0, -1-x+d);
				   if(c > H||d > H){
					   Sumrl++;
					   c=0;
					   d=0;
					   };		   
			}
			if(Sumrl!=0){
				ARL = TR/Sumrl;}
				else ARL=TR;	
			System.out.println("ARL = "+new BigDecimal(ARL).setScale(5, BigDecimal.ROUND_HALF_UP));
		}
	}

	public static  void cusum_k2_0(){
		ARL_0=1;
		TR = 2000000;
		c = 0;
		d = 0;
		x = 0;
		System.out.println("mean shift = 0");
		for(H=0.75;ARL_0<197;H=H+0.005){
			double Sumrl = 0;
			for(int j = 0; j < TR; j++){
			   x = Norm_rand(0);
			   c=Math.max(0, x-2+c);
			   d=Math.max(0, -2-x+d);
			   if(c > H||d > H){
				   Sumrl++;
				   c=0;
				   d=0;
				   };		   
			}
			if(Sumrl!=0){
				ARL_0 = TR/Sumrl;}
				else ARL_0=TR;
		}	
		System.out.println("ARL = "+new BigDecimal(ARL_0).setScale(5, BigDecimal.ROUND_HALF_UP));
		System.out.println("H ="+new BigDecimal(H).setScale(3, BigDecimal.ROUND_HALF_UP));
	}
	
	public static  void cusum_k2_1(){
		TR = 2000000;
		c = 0;
		d = 0;
		x = 0;
		for(double i=0;i<5;i=i+0.1){
			System.out.println("mean shift = "+new BigDecimal(i).setScale(1, BigDecimal.ROUND_HALF_UP));
			double ARL;
			double Sumrl = 0;
			for(int j = 0; j < TR; j++){
				 x = Norm_rand(i);
				   c=Math.max(0, x-2+c);
				   d=Math.max(0, -2-x+d);
				   if(c > H||d > H){
					   Sumrl++;
					   c=0;
					   d=0;
					   };		   
			}
			if(Sumrl!=0){
				ARL = TR/Sumrl;}
				else ARL=TR;	
			System.out.println("ARL = "+new BigDecimal(ARL).setScale(5, BigDecimal.ROUND_HALF_UP));
		}
	}
	
	
	public static double Norm_rand(double miu){
		double N = 12;
		double x=0,temp=N;
		for(int i=0;i<N;i++)
			x=x+(Math.random());
		x=(x-temp/2)/(Math.sqrt(temp/12));
		return x+miu;
		}
}
